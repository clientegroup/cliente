package co.com.ms.ws.ui.controller;


import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import co.com.ms.ws.dto.ClienteDTO;
import co.com.ms.ws.exception.EntityServiceException;
import co.com.ms.ws.service.ClienteService;
import co.com.ms.ws.ui.model.response.Response;

/**
*
* 
* @version 1.0.0
* @author Edinson Orozco González
* @since 27/06/2022
* */
@RequestMapping(path = "${controller.properties.base-path}") 
@RestController

public class ClienteController {

	
	@Autowired
	private ClienteService serviceCliente;
			
	@PostMapping(value = "/createCliente")
    public Response createCliente (@Valid @RequestBody(required= true) ClienteDTO clienteDTO) {
		try {
			serviceCliente.createCliente(clienteDTO);
			return new Response(HttpStatus.OK.value(), "Registro Exitoso", "Guardado Exitoso");
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Guardar el registro", "Error!");
		}
	}
	@GetMapping(value = "/findByClienteId")
	public Response findByIdCliente(@RequestParam String idCliente) {
		try {
			ClienteDTO clienteFind = serviceCliente.findByIdCliente(idCliente);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", clienteFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}
	
	}
	@GetMapping(value = "/findClienteByprimerNombre")
	public Response findClienteByprimerNombre(@RequestParam String primerNombre) {
		try {
			List<ClienteDTO> clienteDTOFind = serviceCliente.findClienteByprimerNombre(primerNombre);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", clienteDTOFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	@GetMapping(value = "/getAllcliente")
	public Response getAllCliente() {
			List<ClienteDTO> listaCliente = serviceCliente.getAll();
			ArrayList<Object> reponseAllConv = new ArrayList<>();
			if (listaCliente!=null && listaCliente.isEmpty())
			{
				return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), reponseAllConv);
			}
			else {
				return new Response(HttpStatus.OK.value(), "Lista Total Exitosa", listaCliente);
			}		
	}
	@PostMapping(value = "/updateCliente")
	public Response updateCliente(@RequestParam String idCliente, @RequestParam String primerNombre, @RequestParam String estado) {
		try {
			ClienteDTO clienteUpdate = serviceCliente.updateCliente(idCliente, primerNombre, estado);
			return new Response(HttpStatus.OK.value(), "Actualizacion Exitoso", clienteUpdate);
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo actualizar el registro", "Error en la Solicitud");
		}
	}
	@PostMapping(value = "/deleteCliente")
	public Response deleteCliente(@RequestParam String idCliente) {
		try {
			ClienteDTO clienteDelete = serviceCliente.deleteCliente(idCliente);
			return new Response(HttpStatus.OK.value(), "Eliminado Exitoso",clienteDelete);
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Eliminar el registro", "Error en la Peticion");
		}
	}
	
	}




	
	
