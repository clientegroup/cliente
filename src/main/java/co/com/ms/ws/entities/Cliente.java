package co.com.ms.ws.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Edinson Orozco González
* @since 27/06/2022
* */
@Getter
@Setter
@Data
@Entity
@Table(name= "cliente", schema = "public")

public class Cliente {
	@Id	
	@Column(name="id_cliente")
	private String idCliente;	
	@NotEmpty(message = "El tipoIdentificacion no puede estar vacio o ser nulo")
	@Pattern(regexp = "^(cedula|pasaporte)$", message = "El tipo de identificación debe ser 'cedula' o 'pasaporte'")
	@Column(name="tipoIdentificacion")
	private String tipoIdentificacion;	
	@NotEmpty(message = "El primerNombre no puede estar vacio o ser nulo")
	@Column(name="primerNombre")
	private String primerNombre;
	@NotEmpty(message = "El segundoNombre no puede estar vacio o ser nulo")
	@Column(name="segundoNombre")
	private String segundoNombre;
	@NotEmpty(message = "El primerApellido no puede estar vacio o ser nulo")
	@Column(name="primerApellido")
	private String primerApellido;
	@NotEmpty(message = "El segundoApellido no puede estar vacio o ser nulo")
	@Column(name="segundoApellido")
	private String segundoApellido;
	@NotEmpty(message = "El telefono no puede estar vacio o ser nulo")
	@Column(name="telefono")
	private String telefono;
	@NotEmpty(message = "La direccionResidencia no puede estar vacio o ser nulo")
	@Column(name="direccionResidencia")
	private String direccionResidencia;
	@NotEmpty(message = "La ciudadResidencia no puede estar vacio o ser nulo")
	@Column(name="ciudadResidencia")
	private String ciudadResidencia;
	@NotEmpty(message = "El estado no puede estar vacio o ser nulo")
	@Column(name="estado")
	private String estado;
}
