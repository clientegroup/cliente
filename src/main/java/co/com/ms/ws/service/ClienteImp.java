package co.com.ms.ws.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import co.com.ms.ws.dto.ClienteDTO;

import co.com.ms.ws.entities.Cliente;
import co.com.ms.ws.repository.ClienteRepository;

@Service
public class ClienteImp implements  ClienteService  {

	@Autowired
	ClienteRepository clienteRepository;
	
	private Logger log = Logger.getLogger("Log Cliente");
	
	public Cliente createCliente (ClienteDTO clienteDTO) {
		log.log(Level.INFO, "Inicio Guardado cliente");
		Cliente cliente = new Cliente();		
		cliente.setIdCliente(clienteDTO.getIdCliente());
		cliente.setTipoIdentificacion(clienteDTO.getTipoIdentificacion());
		cliente.setPrimerNombre(clienteDTO.getPrimerNombre());
		cliente.setSegundoNombre(clienteDTO.getSegundoNombre());
		cliente.setPrimerApellido(clienteDTO.getPrimerApellido());
		cliente.setSegundoApellido(clienteDTO.getSegundoApellido());
		cliente.setTelefono(clienteDTO.getTelefono());
		cliente.setDireccionResidencia(clienteDTO.getDireccionResidencia());
		cliente.setCiudadResidencia(clienteDTO.getCiudadResidencia());
		cliente.setEstado(clienteDTO.getEstado());
		clienteRepository.save(cliente);
		log.log(Level.INFO, "Termina el metodo");		
		return cliente;	
	
	
	}
	public ClienteDTO findByIdCliente(String idCliente) {		
		log.log(Level.INFO, "Inicio busqueda de cliente");
		Cliente clienteFind = clienteRepository.findByIdCliente(idCliente);	
		ClienteDTO clienteDto = ClienteDTO.builder()
					.idCliente(clienteFind.getIdCliente())
					.tipoIdentificacion(clienteFind.getTipoIdentificacion())
					.primerNombre(clienteFind.getPrimerNombre())
					.segundoNombre(clienteFind.getSegundoNombre())
					.primerApellido(clienteFind.getPrimerApellido())
					.segundoApellido(clienteFind.getSegundoApellido())
					.telefono(clienteFind.getTelefono())
					.direccionResidencia(clienteFind.getDireccionResidencia())
					.ciudadResidencia(clienteFind.getCiudadResidencia())	
					.estado(clienteFind.getEstado())
					.build();
			log.log(Level.INFO, "Termina el metodo");
		return clienteDto;
	
	}
	
	public List<ClienteDTO> findClienteByprimerNombre(String primerNombre) {
		log.log(Level.INFO, "Inicio busqueda de cliente");
		List<ClienteDTO> clientesArray = new ArrayList<>();
		List<Cliente> listaClientes = clienteRepository.findClienteByprimerNombre(primerNombre);
		
		for (Cliente clienteEach: listaClientes) {
			ClienteDTO clienteDto = ClienteDTO.builder()
				.idCliente(clienteEach.getIdCliente())
				.estado(clienteEach.getEstado())
				.build();
			clientesArray.add(clienteDto);
		}
		log.log(Level.INFO, "Termina Metodo");
		return clientesArray;
	}

	public List<ClienteDTO> getAll() {
		log.log(Level.INFO, "Inicio listado de clientes");
		List<ClienteDTO> clienteArray = new ArrayList<>();
		List<Cliente> listaClientes = clienteRepository.findAll();
		
		for (Cliente clienteEach: listaClientes) {			
			ClienteDTO clienteDto = ClienteDTO.builder()
					.idCliente(clienteEach.getIdCliente())
					.tipoIdentificacion(clienteEach.getTipoIdentificacion())
					.primerNombre(clienteEach.getPrimerNombre())
					.segundoNombre(clienteEach.getSegundoNombre())
					.primerApellido(clienteEach.getPrimerApellido())
					.segundoApellido(clienteEach.getSegundoApellido())
					.telefono(clienteEach.getTelefono())
					.direccionResidencia(clienteEach.getDireccionResidencia())
					.ciudadResidencia(clienteEach.getCiudadResidencia())
					.estado(clienteEach.getEstado())
					.build();
			clienteArray.add(clienteDto);
		}
		log.log(Level.INFO, "Termina el metodo");
		return clienteArray;
	}
	public ClienteDTO updateCliente(String idCliente, String primerNombre, String estado) {

		log.log(Level.INFO, "Inicio Actualizacion de cliente");		
		Cliente clienteUpdate = clienteRepository.findByIdCliente(idCliente);
		clienteUpdate.setPrimerNombre(primerNombre);
		clienteUpdate.setEstado(estado);	
		ClienteDTO clienteDto = ClienteDTO.builder()
				.idCliente(clienteUpdate.getIdCliente())
				.tipoIdentificacion(clienteUpdate.getTipoIdentificacion())
				.primerNombre(clienteUpdate.getPrimerNombre())
				.segundoNombre(clienteUpdate.getSegundoNombre())
				.primerApellido(clienteUpdate.getPrimerApellido())
				.segundoApellido(clienteUpdate.getSegundoApellido())
				.telefono(clienteUpdate.getTelefono())
				.direccionResidencia(clienteUpdate.getDireccionResidencia())
				.ciudadResidencia(clienteUpdate.getCiudadResidencia())
				.estado(clienteUpdate.getEstado())
				.build();
		clienteRepository.save(clienteUpdate);
		log.log(Level.INFO, "Finaliza");

		return clienteDto;
	}
	
	public ClienteDTO deleteCliente(String idCliente) {

		log.log(Level.INFO, "Inicio Borrado de cliente");
		Cliente clienteDelete = clienteRepository.findByIdCliente(idCliente);
		ClienteDTO clienteDto = ClienteDTO.builder()
				.idCliente(clienteDelete.getIdCliente())
				.primerNombre(clienteDelete.getPrimerNombre())
				.estado(clienteDelete.getEstado())
				.build();

		clienteRepository.delete(clienteDelete);

		log.log(Level.INFO, "Finalizacion del metodo");

		return clienteDto;
	}
	
}
