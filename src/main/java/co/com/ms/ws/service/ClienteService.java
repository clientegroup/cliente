package co.com.ms.ws.service;

import java.util.List;

import co.com.ms.ws.dto.ClienteDTO;
import co.com.ms.ws.entities.Cliente;

public interface ClienteService {

	Cliente createCliente (ClienteDTO clienteDTO);
	ClienteDTO findByIdCliente(String idCliente);
	List<ClienteDTO> findClienteByprimerNombre(String primerNombre);
	List<ClienteDTO> getAll();
	ClienteDTO updateCliente(String idCliente, String primerNombre, String estado);
	ClienteDTO deleteCliente(String idCliente);
}
