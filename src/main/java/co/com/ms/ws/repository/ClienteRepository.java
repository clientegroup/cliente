package co.com.ms.ws.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import co.com.ms.ws.entities.Cliente;
/**
*
* 
* @version 1.0.0
* @author Edinson Orozco González
* @since 27/06/2022
* */
public interface ClienteRepository extends JpaRepository<Cliente, String> {
	
	Cliente findByIdCliente (String idCliente);
	
	List<Cliente> findClienteByidCliente (String idCliente);
	
	List<Cliente> findClienteByprimerNombre (String primerNombre);
}
