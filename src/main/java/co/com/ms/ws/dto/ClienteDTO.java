package co.com.ms.ws.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Edinson Orozco González
* @since 27/06/2022
* */
@Builder
@Setter
@Getter
@AllArgsConstructor()

public class ClienteDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	private String idCliente;
	private String tipoIdentificacion;
	private String primerNombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private String telefono;
	private String direccionResidencia;
	private String ciudadResidencia;
	private String estado;
	
	public ClienteDTO() {
}
}